﻿using System.IO;
using EasyNetworking;

namespace TestApp
{
    /// <summary>
    /// @TODO {{{ TEST CLASS }}}
    /// Serializes a string into the binary writer.
    /// </summary>
    public class StringBinarySerializable : IBinarySerializable
    {
        public readonly string Message;

        public StringBinarySerializable(string message)
        {
            Message = message;
        }

        public void Serialize(BinaryWriter writer)
        {
            writer.Write(Message);
        }
    }
}
