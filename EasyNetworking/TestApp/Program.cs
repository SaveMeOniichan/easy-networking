﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using EasyNetworking;

namespace TestApp
{
    class Program
    {
        const int PORT_NO = 46531;
        const string SERVER_IP = "127.0.0.1";

        public delegate void Act(string s);

        public static event Act act2;

        static void Main(string[] args)
        {
            Action<string>[] act1 = new Action<string>[1];
            act1[0] = (x) => { x = x + "1"; };
            //test[1] = (x) => { x = x + "1"; };
            //test[2] = (x) => { x = x + "1"; };

            act2 += (x) => { x = x + "1"; };
            //act2 += (x) => { x = x + "1"; };
            //act2 += (x) => { x = x + "1"; };

            Event<string> act3 = new Event<string>();
            act3.Add((x) => { x = x + "1"; });
            //act3.Add((x) => { x = x + "1"; });
            //act3.Add((x) => { x = x + "1"; });

            Event2<string> act4 = new Event2<string>();
            act4.Add((x) => { x = x + "1"; });
            //act4.Add((x) => { x = x + "1"; });
            //act4.Add((x) => { x = x + "1"; });

            Stopwatch s;

            s = new Stopwatch();
            s.Start();
            for (int i = 0; i < 1000; i++)
            {
                for (int j = 0; j < act1.Length; j++)
                    act1[j]("Hello World");
            }
            s.Stop();
            Console.WriteLine(s.ElapsedNanoSeconds());

            s = new Stopwatch();
            s.Start();
            for (int i = 0; i < 1000; i++)
            {
                act2("Hello World");
            }
            s.Stop();
            Console.WriteLine(s.ElapsedNanoSeconds());

            s = new Stopwatch();
            s.Start();
            for (int i = 0; i < 1000; i++)
            {
                act3.Invoke("Hello World");
            }
            s.Stop();
            Console.WriteLine(s.ElapsedNanoSeconds());

            s = new Stopwatch();
            s.Start();
            for (int i = 0; i < 1000; i++)
            {
                act4.Invoke("Hello World");
            }
            s.Stop();
            Console.WriteLine(s.ElapsedNanoSeconds());

            Console.ReadKey();
            
        }

        //static void listen()
        //{
        //    //IPAddress localAdd = IPAddress.Parse(SERVER_IP);
        //    //IPEndPoint end = new IPEndPoint(localAdd, PORT_NO);

        //    //TcpListener listener = new TcpListener(end);
        //    //listener.Start();
        //    //TcpClient client = listener.AcceptTcpClient();
        //    //TcpConnection connection = new TcpConnection(client, ReadMessage2);

        //    UdpClient listener = new UdpClient(PORT_NO);

        //    IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any, 8000);
        //    byte[] data = listener.Receive(ref remoteEP);

        //    listener.Connect(remoteEP);

        //    UdpConnection connection = new UdpConnection(listener, ReadMessage2);

        //    connection.Start();

        //    //for (int i = 0; i < 10; i++)
        //    //{
        //    //    connection.Send(GetTestMessage());
        //    //    Thread.Sleep(500);
        //    //}
        //}

        //static StringBinarySerializable GetTestMessage()
        //{
        //    return new StringBinarySerializable(KeyGenerator.Get64ByteUnqiueKey());
        //}

        //static void ReadMessage1(BinaryReader reader)
        //{
        //    string message = reader.ReadString();
        //    Console.WriteLine("1: " + message);
        //}
        //static void ReadMessage2(BinaryReader reader)
        //{
        //    string message = reader.ReadString();
        //    Console.WriteLine("2: " + message);
        //}
    }

    public static class Utility
    {
        public static long ElapsedNanoSeconds(this Stopwatch watch)
        {
            return watch.ElapsedTicks * 1000000000 / Stopwatch.Frequency;
        }
        public static long ElapsedMicroSeconds(this Stopwatch watch)
        {
            return watch.ElapsedTicks * 1000000 / Stopwatch.Frequency;
        }
    }
}
