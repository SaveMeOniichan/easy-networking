﻿using System.Security;

namespace EasyNetworking.Unsafe
{
    public static class ReverseBitConveter
    {
        [SecuritySafeCritical]
        public static byte[] GetBytes(int value)
        {
            unsafe
            {
                byte* numPointer;
                byte[] numArray = new byte[4];
                byte[] numArray1 = numArray;
                if (numArray == null || (int)numArray1.Length == 0)
                {
                    numPointer = null;
                }
                else
                {
                    numPointer = &numArray1[0];
                }
                *numPointer = (byte)value;
                numPointer = null;
                return numArray;
            }
        }
    }
}
