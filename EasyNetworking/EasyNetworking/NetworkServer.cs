﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using EasyNetworking.Connections;
using EasyNetworking.Utils;

namespace EasyNetworking
{
    public class NetworkServer
    {
        private int _networkIdCounter = 1;

        public readonly IPEndPoint ServerEndpoint;

        public readonly int Port;

        public bool IsRunning { get; private set; }

        public IDictionary<IPEndPoint, NetworkConnection> _connectionsByEndpoint = new Dictionary<IPEndPoint, NetworkConnection>();
        public IDictionary<int, NetworkConnection> _connectionsById = new Dictionary<int, NetworkConnection>();


        public NetworkServer(IPEndPoint serverEndpoint)
        {
            IsRunning = false;
            ServerEndpoint = serverEndpoint;
            Port = serverEndpoint.Port;
        }


        public NetworkConnection GetNetworkConnection(int networkId)
        {
            return _connectionsById[networkId];
        }

        public NetworkConnection GetNetworkConnection(IPEndPoint endpoint)
        {
            return _connectionsByEndpoint[endpoint];
        }

        private void TcpAcceptorJob()
        {
            TcpListener tcpListener = new TcpListener(ServerEndpoint);
            while (true)
            {
                try
                {
                    TcpClient client = tcpListener.AcceptTcpClient();
                    TcpConnection tcpConnection = new TcpConnection(client, TcpMessageReceived);

                    UdpConnection udpConnection;
                    try
                    {
                        // Try/catch because we need to connect a UdpClient to the endpoint.
                        udpConnection = new UdpConnection(tcpConnection.Endpoint);
                        
                        NetworkConnection networkConnection = new NetworkConnection(
                            _networkIdCounter++,
                            tcpConnection.Endpoint,
                            tcpConnection,
                            udpConnection
                        );

                        _connectionsByEndpoint.Add(networkConnection.Endpoint, networkConnection);
                        _connectionsById.Add(networkConnection.NetworkId, networkConnection);

                        if (OnConnect != null)
                        {
                            OnConnect(networkConnection);
                        }
                    }
                    catch (Exception e)
                    {
                        // We failed to establish a Udp connection.
                        if (OnFailureToEstablishUdp != null)
                        {
                            OnFailureToEstablishUdp(tcpConnection.Endpoint);
                        }
                    }
                }
                catch (Exception e)
                {
                    
                }
            }
        }

        private void TcpMessageReceived(BinaryReader message)
        {

        }

        #region Events

        #region OnConnect

        /// <summary>
        /// The event fired when a connection is made to the server.
        /// </summary>
        /// <param name="connection">Connection that was made.</param>
        public delegate void OnConnectAction(NetworkConnection connection);

        /// <summary>
        /// The event fired when a connection is made to the server.
        /// </summary>
        public event OnConnectAction OnConnect;

        #endregion

        #region OnDisconnect

        /// <summary>
        /// The event fired when a connection disconnects from the server.
        /// </summary>
        /// <param name="connection">Connection that was disconnected.</param>
        public delegate void OnDisconnectAction(NetworkConnection connection);

        /// <summary>
        /// The event fired when a connection disconnects from the server.
        /// </summary>
        public event OnDisconnectAction OnDisconnect;

        #endregion

        #region OnFailureToEstablishUdp

        /// <summary>
        /// The event fired when a Tcp connect comes in and the server fails to connect to that client with Udp.
        /// </summary>
        /// <param name="endpoint">Endpoint that the Udp couldn't connect to.</param>
        public delegate void OnFailureToEstablishUdpAction(IPEndPoint endpoint);

        /// <summary>
        /// The event fired when a Tcp connect comes in and the server fails to connect to that client with Udp.
        /// </summary>
        public event OnFailureToEstablishUdpAction OnFailureToEstablishUdp;

        #endregion

        #endregion
    }
}
