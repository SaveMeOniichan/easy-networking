﻿using System.IO;

namespace EasyNetworking
{
    /// <summary>
    /// Allows for the object to be serialized into a BinaryWriter.
    /// How it is serialized is done by the implementation of this interface.
    /// </summary>
    public interface IBinarySerializable
    {
        /// <summary>
        /// Serialize an object into the binary writer.
        /// </summary>
        /// <param name="writer">Writer to write to.</param>
        void Serialize(BinaryWriter writer);
    }
}
