﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Net;
//using System.Net.Sockets;

//namespace EasyNetworking
//{
//    /// <summary>
//    /// This class creates UdpConnection objects.
//    /// It takes a UdpClient and continuely listens for messages it will connect it.
//    /// </summary>
//    public class UdpConnector
//    {

//        #region Public - Fields and Properties

//        /// <summary>
//        /// Whether the connector is currently creating connections.
//        /// </summary>
//        public bool IsRunning { get; private set; }

//        /// <summary>
//        /// The local port of the UdpClient being used as a listener.
//        /// </summary>
//        public int Port { get; private set; }

//        public UdpClient ListenerClient { get; private set; }

//        #endregion

//        #region Private - Fields and Properties

//        private IDictionary<IPEndPoint, UdpConnection> _connected;

//        private readonly ActionThreadJob _connectingJob;
//        private readonly Action<UdpConnection> _connectedCallback;
//        private readonly Action<BinaryReader> _messageCallback;

//        #endregion

//        #region Constructors

//        public UdpConnector(UdpClient udpClientListener, Action<UdpConnection> connectedCallback, Action<BinaryReader> messageCallback)
//        {
//            _connectedCallback = connectedCallback;
//            _messageCallback = messageCallback;
//            ListenerClient = udpClientListener;

//            _connected = new Dictionary<IPEndPoint, UdpConnection>();

//            _connectingJob = new ActionThreadJob(RunUdpConnector);
//        }

//        #endregion

//        #region Public - Methods
        
//        public UdpConnection GetConnection(IPEndPoint endpoint)
//        {
//            UdpConnection connection;
//            if (!_connected.TryGetValue(endpoint, out connection))
//            {
//                // TODO throw exception
//                return null;
//            }
//            return connection;
//        }

//        public void Start()
//        {
//            IsRunning = true;
//            _connectingJob.Start();
//        }

//        public void Stop()
//        {
//            IsRunning = false;
//        }

//        #endregion

//        #region Private - Methods

//        private void RunUdpConnector()
//        {
//            while (IsRunning)
//            {
//                // Try to receive data on the UDP port, when we do, we got a new user.
//                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any, Port);
//                byte[] data = ListenerClient.Receive(ref remoteEP);

//                // We can ignore all messages 
//                if (!IsRunning || _connected.ContainsKey(remoteEP))
//                {
//                    return;
//                }
                
//                // This will serve as the ListenerClient after the current one connects to the incoming message.
//                var nextListenerClient = new UdpClient(ListenerClient.Client.AddressFamily);
                
//                MemoryStream memStream = new MemoryStream(data);
//                BinaryReader binaryReader = new BinaryReader(memStream);
//                _messageCallback(binaryReader);

//                ListenerClient.Connect(remoteEP);
//                UdpConnection connection = new UdpConnection(ListenerClient, _messageCallback);
//                connection.Start();

//                _connected.Add(remoteEP, connection);

//                _connectedCallback(connection);

//                ListenerClient = nextListenerClient;
//            }
//        }

//        #endregion
//    }
//}
