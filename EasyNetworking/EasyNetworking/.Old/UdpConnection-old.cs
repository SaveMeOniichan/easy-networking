﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Net;
//using System.Net.Sockets;
//using EasyNetworking.Exceptions;

//namespace EasyNetworking
//{
//    /// <summary>
//    /// A UDP "connection" over a UdpClient.
//    /// In quotations because UDP technically doesn't have "connections".
//    /// This class manages sending and receiving messages.
//    /// </summary>
//    public class UdpConnection : IConnection
//    {
//        #region Public - Properties and Fields

//        /// <summary>
//        /// The UdpClient used for UDP messages.
//        /// </summary>
//        public UdpClient Client { get; private set; }

//        /// <summary>
//        /// If this connection is still running.
//        /// </summary>
//        public bool IsRunning { get; private set; }

//        /// <summary>
//        /// The port that this connection is listening on.
//        /// </summary>
//        public int Port { get; private set; }

//        #endregion

//        #region Private - Properties and Fields
        
//        /// <summary>
//        /// The queue of messages to send over UDP.
//        /// </summary>
//        private readonly Queue<IBinarySerializable> _messageQueue;

//        /// <summary>
//        /// A lock for adding to and removing from the message queue.
//        /// </summary>
//        private readonly object _messageQueueLock = new object();

//        /// <summary>
//        /// The callback that will be used for reading received messages.
//        /// </summary>
//        private readonly Action<BinaryReader> _readCallback;

//        /// <summary>
//        /// The job that is used for processing messages received.
//        /// </summary>
//        private readonly ActionThreadJob _readingJob;

//        /// <summary>
//        /// The job that is used for processing messages being sent.
//        /// </summary>
//        private readonly ActionThreadJob _writingJob;

//        #endregion

//        #region Constructors

//        /// <summary>
//        /// Constructs a UdpConnection that connects to the IP EndPoint.
//        /// </summary>
//        /// <param name="ipEndPoint">Address to connect to with the UdpClient.</param>
//        /// <param name="readCallback">Where received messages will be forwarded to.</param>
//        public UdpConnection(IPEndPoint ipEndPoint, Action<BinaryReader> readCallback) : this(readCallback)
//        {
//            // Create a UdpClient and connect it
//            Client = new UdpClient();
//            Client.Connect(ipEndPoint);
//            Port = ((IPEndPoint)Client.Client.LocalEndPoint).Port;
//        }

//        /// <summary>
//        /// Constructs a UdpConnection that connects to the hostname and port provided.
//        /// </summary>
//        /// <param name="hostName">The host that will be connected to.</param>
//        /// <param name="port">The port to connect on.</param>
//        /// <param name="readCallback">Where received messages will be forwarded to.</param>
//        public UdpConnection(string hostName, int port, Action<BinaryReader> readCallback) : this(readCallback)
//        {
//            // Create a UdpClient and connect it
//            Client = new UdpClient();
//            Client.Connect(hostName, port);
//            Port = ((IPEndPoint)Client.Client.LocalEndPoint).Port;
//        }

//        /// <summary>
//        /// Constructs a UdpConnection using an existing UdpClient.
//        /// </summary>
//        /// <param name="client">The UdpClient that will be used in the connection.</param>
//        /// <param name="readCallback">Where received messages will be forwarded to.</param>
//        public UdpConnection(UdpClient client, Action<BinaryReader> readCallback) : this(readCallback)
//        {
//            // It's assumed that the client is already connected.
//            Client = client;
//            Port = ((IPEndPoint) Client.Client.LocalEndPoint).Port;
//        }

//        /// <summary>
//        /// Private constructor used to handle the non-client setup.
//        /// </summary>
//        /// <param name="readCallback">Where received messages will be forwarded to.</param>
//        private UdpConnection(Action<BinaryReader> readCallback)
//        {
//            IsRunning = false;
            
//            _messageQueue = new Queue<IBinarySerializable>();

//            // Were we will send received messages to.
//            _readCallback = readCallback;

//            // Create our jobs that we'll start later when we call Start()
//            _readingJob = new ActionThreadJob(ReadingJob);
//            _writingJob = new ActionThreadJob(WritingJob);
//        }

//        #endregion

//        #region Public Methods

//        /// <summary>
//        /// Writes the given message to the underlying UDP connection.
//        /// </summary>
//        /// <param name="message">Message that will be send over the connection.</param>
//        /// <exception cref="ConnectionNotRunningException">Thrown if the UdpConnection is not running.</exception>
//        public void Send(IBinarySerializable message)
//        {
//            if (!IsRunning)
//            {
//                throw new ConnectionNotRunningException();
//            }

//            lock (_messageQueueLock)
//            {
//                _messageQueue.Enqueue(message);
//            }
//        }

//        /// <summary>
//        /// Starts the connection.
//        /// </summary>
//        public void Start()
//        {
//            IsRunning = true;
//            _readingJob.Start();
//            _writingJob.Start();
//        }

//        /// <summary>
//        /// Stops the connection.
//        /// All queued messages are stopped.
//        /// </summary>
//        /// <exception cref="ConnectionNotRunningException">Thrown if the UdpConnection is not running.</exception>
//        public void Stop()
//        {
//            if (!IsRunning)
//            {
//                throw new ConnectionNotRunningException();
//            }

//            IsRunning = false;
//            lock (_messageQueueLock)
//            {
//                _messageQueue.Clear();
//            }
//        }

//        #endregion

//        #region Private Methods

//        /// <summary>
//        /// The job the is responsible for reading from the UDPClient.
//        /// </summary>
//        /// <exception cref="PacketSizeException">Thrown if a message that is larger than the specified limit is received.</exception>
//        private void ReadingJob()
//        {
//            Client.BeginReceive(RecieveMessage, null);
//        }

//        /// <summary>
//        /// The job that is responsible for writing to the UDPClient.
//        /// </summary>
//        private void WritingJob()
//        {
//            while (IsRunning)
//            {
//                IBinarySerializable message = GetNextMessageInWritingQueue();

//                // Wait until there is a message to send.
//                if (message == null)
//                {
//                    continue;
//                }
                
//                // Unlike TCP which deals in streams forcing us to send packet sizes, UDP sends a datagram which will be recieved whole (if they are received at all)
//                MemoryStream stream = new MemoryStream();
//                BinaryWriter writer = new BinaryWriter(stream);
//                message.Serialize(writer);
//                byte[] bytes = stream.ToArray();
//                // That means we don't have to write any message lengths, and instead can just send the message itself.
//                Client.Send(bytes, bytes.Length);
//            }
//        }

//        /// <summary>
//        /// Returns the next message to be written over the connection.
//        /// If there is no messages in the queue, then null will be returned.
//        /// </summary>
//        /// <returns>Next message dequeued.</returns>
//        private IBinarySerializable GetNextMessageInWritingQueue()
//        {
//            IBinarySerializable message;
//            lock (_messageQueueLock)
//            {
//                if (_messageQueue.Count > 0)
//                {
//                    message = _messageQueue.Dequeue();
//                }
//                else
//                {
//                    message = null;
//                }
//            }
//            return message;
//        }

//        /// <summary>
//        /// Used for receiving a single message from the UDP connection.
//        /// </summary>
//        /// <param name="message">message received.</param>
//        private void RecieveMessage(IAsyncResult message)
//        {
//            if (IsRunning)
//            {
//                // Retreive the message that was sent to the Udp socket
//                IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, Port);
//                byte[] buffer = Client.EndReceive(message, ref RemoteIpEndPoint);

//                // Begin receiving again
//                Client.BeginReceive(RecieveMessage, null);

//                // Construct the reader to send to the callback
//                MemoryStream stream = new MemoryStream(buffer);
//                BinaryReader reader = new BinaryReader(stream);

//                // Send it to the reading callback
//                _readCallback(reader);
//            }
//        }

//        #endregion
//    }
//}
