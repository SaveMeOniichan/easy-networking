﻿using System;
using System.Collections.Generic;

namespace EasyNetworking
{
    public class Event<T> : List<Action<T>>
    {
        public void Invoke(T message)
        {
            for (int i = 0; i < Count; i++)
            {
                this[i](message);
            }
        }
    }

    public class Event2<T>
    {
        public Action<T>[] _actions = new Action<T>[0];

        public void Invoke(T message)
        {
            for (int i = 0; i < _actions.Length; i++)
            {
                _actions[i](message);
            }
        }

        public void Add(Action<T> callback)
        {
            Action<T>[] old = _actions;
            _actions = new Action<T>[old.Length + 1];
            old.CopyTo(_actions, 0);
            _actions[_actions.Length - 1] = callback;
        }
    }
}
