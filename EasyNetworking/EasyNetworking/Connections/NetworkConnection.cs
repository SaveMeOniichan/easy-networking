﻿using System.Net;
using EasyNetworking.Utils;

namespace EasyNetworking.Connections
{
    public class NetworkConnection
    {
        public readonly int NetworkId;

        public readonly int Port;

        public readonly IPEndPoint Endpoint;

        public readonly TcpConnection Tcp;

        public readonly UdpConnection Udp;

        public NetworkConnection(int networkId, IPEndPoint endpoint, TcpConnection tcp, UdpConnection udp)
        {
            NetworkId = networkId;
            Endpoint = endpoint;
            Port = endpoint.Port;
            Tcp = tcp;
            Udp = udp;
        }
    }
}
