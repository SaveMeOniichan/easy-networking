﻿using System.Security.Cryptography;
using System.Text;

namespace EasyNetworking.Utils
{
    /// <summary>
    /// A static class for generating random unqiue keys.
    /// </summary>
    public static class KeyGenerator
    {
        /// <summary>
        /// Creates a random unique key of the given size.
        /// Key's total byte size with be 2*maxSize.
        /// </summary>
        /// <param name="maxSize">Size of the key to generate. Will contain this many characters.</param>
        /// <returns>Key created.</returns>
        public static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetBytes(data);
            data = new byte[maxSize];
            crypto.GetBytes(data);
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        #region Shortcut Methods

        /// <summary>
        /// Creates a random unique key that is 16 bytes in length.
        /// </summary>
        /// <returns>Key created.</returns>
        public static string Get16ByteUnqiueKey()
        {
            return GetUniqueKey(8);
        }

        /// <summary>
        /// Creates a random unique key that is 32 bytes in length.
        /// </summary>
        /// <returns>Key created.</returns>
        public static string Get32ByteUnqiueKey()
        {
            return GetUniqueKey(16);
        }

        /// <summary>
        /// Creates a random unique key that is 64 bytes in length.
        /// </summary>
        /// <returns>Key created.</returns>
        public static string Get64ByteUnqiueKey()
        {
            return GetUniqueKey(32);
        }

        #endregion
    }
}