﻿using System;

namespace EasyNetworking.Utils
{
    /// <summary>
    /// A ThreadJob that runs an Action passed into its contructor.
    /// </summary>
    public class ActionThreadJob : ThreadJob
    {
        /// <summary>
        /// The action run by the ThreadJob.
        /// </summary>
        private readonly Action _threadJobAction;

        /// <summary>
        /// The action run after the main action has finished running.
        /// </summary>
        private readonly Action _threadFinishedCallbackAction;

        /// <summary>
        /// Constructs a ThreadJob that uses the provided Action as it's Run function.
        /// Null actions parameters will not be called.
        /// </summary>
        /// <param name="threadJobAction">The action to be executed on a Thread.</param>
        /// <param name="threadFinishedCallbackAction">Called immediately after the first action, useful for callbacks.</param>
        public ActionThreadJob(Action threadJobAction, Action threadFinishedCallbackAction = null)
        {
            _threadJobAction = threadJobAction;
            _threadFinishedCallbackAction = threadFinishedCallbackAction;
        }

        public override void Run()
        {
            if (_threadJobAction != null)
            {
                _threadJobAction();
            }
        }

        public override void Finish()
        {
            if (_threadFinishedCallbackAction != null)
            {
                _threadFinishedCallbackAction();
            }
        }
    }
}
