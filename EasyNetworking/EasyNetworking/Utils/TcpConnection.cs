﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using EasyNetworking.Exceptions;

namespace EasyNetworking.Utils
{
    /// <summary>
    /// A TCP connection over a TcpClient.
    /// This class manages sending and receiving messages.
    /// </summary>
    public class TcpConnection
    {
        /// <summary>
        /// Packets cannot be larger than this size or an exception will be thrown.
        /// </summary>
        public const int DEFAULT_MAX_PACKET_SIZE = 16384;

        #region Public - Properties and Fields

        /// <summary>
        /// The TcpClient used for TCP messages.
        /// </summary>
        public readonly TcpClient Client;

        /// <summary>
        /// The endpoint used for the Tcp connection.
        /// </summary>
        public readonly IPEndPoint Endpoint;

        /// <summary>
        /// The port used for the Tcp connection.
        /// </summary>
        public readonly int Port;

        /// <summary>
        /// If this connection is still running.
        /// </summary>
        public bool IsRunning { get; private set; }

        /// <summary>
        /// The maximum packet size acceptable.
        /// Defaults to DEFAULT_MAX_PACKET_SIZE.
        /// </summary>
        public int MaxPacketSize { get; set; }

        #endregion

        #region Private - Properties and Fields

        /// <summary>
        /// The underlying Stream from the TcpClient.
        /// </summary>
        private readonly Stream _stream;

        /// <summary>
        /// The queue of messages to send over TCP.
        /// </summary>
        private readonly Queue<IBinarySerializable> _messageQueue;

        /// <summary>
        /// A lock for adding to and removing from the message queue.
        /// </summary>
        private readonly object _messageQueueLock = new object();

        /// <summary>
        /// The callback that will be used for reading received messages.
        /// </summary>
        private readonly Action<BinaryReader> _readCallback;

        /// <summary>
        /// The job that is used for processing messages received.
        /// </summary>
        private readonly ActionThreadJob _readingJob;

        /// <summary>
        /// The job that is used for processing messages being sent.
        /// </summary>
        private readonly ActionThreadJob _writingJob;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a TcpConnection using an existing TcpClient.
        /// </summary>
        /// <param name="client">The TcpClient that will be used in the connection.</param>
        /// <param name="readCallback">Where received messages will be forwarded to.</param>
        public TcpConnection(TcpClient client, Action<BinaryReader> readCallback)
        {
            Client = client;
            Client.NoDelay = true;
            Endpoint = ((IPEndPoint) Client.Client.RemoteEndPoint);
            Port = Endpoint.Port;
            _stream = Client.GetStream();

            IsRunning = false;

            _messageQueue = new Queue<IBinarySerializable>();

            // Were we will send received messages to.
            _readCallback = readCallback;

            // Create our jobs that we'll start later when we call Start()
            _readingJob = new ActionThreadJob(ReadingJob);
            _writingJob = new ActionThreadJob(WritingJob);

            MaxPacketSize = DEFAULT_MAX_PACKET_SIZE;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Writes the given message to the underlying TCP connection.
        /// </summary>
        /// <param name="message">Message that will be send over the connection.</param>
        /// <exception cref="ConnectionNotRunningException">Thrown if the TcpConnection is not running.</exception>
        public void Send(IBinarySerializable message)
        {
            if (!IsRunning)
            {
                throw new ConnectionNotRunningException();
            }

            lock (_messageQueueLock)
            {
                _messageQueue.Enqueue(message);
            }
        }

        /// <summary>
        /// Starts the connection.
        /// </summary>
        public void Start()
        {
            IsRunning = true;
            _readingJob.Start();
            _writingJob.Start();
        }

        /// <summary>
        /// Stops the connection.
        /// All queued messages are stopped.
        /// </summary>
        /// <exception cref="ConnectionNotRunningException">Thrown if the TcpConnection is not running.</exception>
        public void Stop()
        {
            if (!IsRunning)
            {
                throw new ConnectionNotRunningException();
            }

            IsRunning = false;
            lock (_messageQueueLock)
            {
                _messageQueue.Clear();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// The job the is responsible for reading from the TcpClient.
        /// </summary>
        /// <exception cref="PacketSizeException">Thrown if a message that is larger than the specified limit is received.</exception>
        private void ReadingJob()
        {
            BinaryReader reader = new BinaryReader(_stream);
            while (IsRunning)
            {
                // We have decided from a design standpoint to send an integer with every message at the beginning.
                // The integer indicates the size of the TCP message because TCP messages aren't received all at once.
                // We will wait until we get the first 4 bytes (4 bytes in an int) so we can tell how large the message
                // should be.
                // The process of waiting for the entire message is called "Message Farming"
                while (Client.Available < 4 && IsRunning)
                {
                    continue;
                }

                // Now we need to read our integer
                byte[] packetSizeBuffer = new byte[4];
                reader.Read(packetSizeBuffer, 0, 4);
                int packetSize = BitConverter.ToInt32(packetSizeBuffer, 0);

                if (packetSize > MaxPacketSize)
                {
                    Client.Close();
                    throw new PacketSizeException(packetSize, MaxPacketSize);
                }

                // Now that we know how big our 'packet' is supposed to be, we wait until we recieve the full packet
                while (Client.Available < packetSize && IsRunning)
                {
                    continue;
                }

                // Now we can read our entire packet
                byte[] packet = new byte[packetSize];
                reader.Read(packet, 0, packetSize);

                // Finally, stick it into a memory stream so that it can be read by a binary reader and passed on to the callback
                MemoryStream memoryStream = new MemoryStream(packet);
                BinaryReader memoryReader = new BinaryReader(memoryStream);

                // This is the total message that we just received.
                _readCallback(memoryReader);
            }
        }

        /// <summary>
        /// The job that is responsible for writing to the TcpClient.
        /// </summary>
        private void WritingJob()
        {
            while (IsRunning)
            {
                IBinarySerializable message = GetNextMessageInWritingQueue();

                // Wait until there is a message to send.
                if (message == null)
                {
                    continue;
                }
                
                // Serialize the message into a byte[]
                MemoryStream memStream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(memStream);
                message.Serialize(writer);
                byte[] messageBytes = memStream.ToArray();

                // We append the length of the message we're sending to the front of each message.
                // We need the size of the entire message for "Message Farming" on the receiving end.
                byte[] packetSize = BitConverter.GetBytes(messageBytes.Length);

                // First we write the packet size we'll be sending.
                _stream.Write(packetSize, 0, 4);
                // Then write the message itself.
                _stream.Write(messageBytes, 0, messageBytes.Length);
            }
        }

        /// <summary>
        /// Returns the next message to be written over the connection.
        /// If there is no messages in the queue, then null will be returned.
        /// </summary>
        /// <returns>Next message dequeued.</returns>
        private IBinarySerializable GetNextMessageInWritingQueue()
        {
            IBinarySerializable message;
            lock (_messageQueueLock)
            {
                if (_messageQueue.Count > 0)
                {
                    message = _messageQueue.Dequeue();
                }
                else
                {
                    message = null;
                }
            }
            return message;
        }

        #endregion
    }
}
