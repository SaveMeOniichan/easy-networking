﻿using System.Net;
using System.Net.Sockets;

namespace EasyNetworking.Utils
{
    public class UdpConnection
    {
        public readonly IPEndPoint Endpoint;

        public readonly int Port;

        public UdpClient Client;

        public UdpConnection(IPEndPoint endpoint)
        {
            Endpoint = endpoint;
            Port = Endpoint.Port;

            // Create and connect the Udp.
            Client = new UdpClient();
            Client.Connect(endpoint);
        }
    }
}
