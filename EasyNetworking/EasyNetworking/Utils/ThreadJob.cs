﻿using System;
using System.Collections;
using System.Threading;
using EasyNetworking.Exceptions;

namespace EasyNetworking.Utils
{
    /// <summary>
    /// AA wrapper for a job that will be executed on another thread.
    /// Useful for long running processes.
    /// </summary>
    public abstract class ThreadJob
    {
        #region Fields and Properties

        /// <summary>
        /// A lock for accessing the IsDone field.
        /// </summary>
        private readonly object _handleIsDone = new object();

        /// <summary>
        /// Whether or not the ThreadJob has started already.
        /// </summary>
        public bool IsStarted { get; private set; }

        /// <summary>
        /// Whether or not the ThreadJob is completed or not.
        /// </summary>
        public bool IsDone
        {
            get
            {
                bool tmp;
                lock (_handleIsDone)
                {
                    tmp = _isDone;
                }
                return tmp;
            }
            private set
            {
                lock (_handleIsDone)
                {
                    _isDone = value;
                }
            }
        }
        private bool _isDone = false;

        #endregion

        #region Methods

        /// <summary>
        /// The action that will be run by the ThreadJob.
        /// </summary>
        public abstract void Run();

        /// <summary>
        /// An action called immediately after the Run function is finished.
        /// </summary>
        public abstract void Finish();

        /// <summary>
        /// Starts the ThreadJob.
        /// A ThreadJob can only be started once.
        /// </summary>
        /// <exception cref="ThreadJobAlreadyStartedException">Thrown if you try to Start a ThreadJob that has already started.</exception>
        /// <exception cref="NotSupportedException">Thrown if the ThreadJob could not be queued.</exception>
        public void Start()
        {
            if (!IsStarted)
            {
                ThreadPool.QueueUserWorkItem(ExecuteThread);
                IsStarted = true;
            }
            else
            {
                throw new ThreadJobAlreadyStartedException(this);
            }
        }

        /// <summary>
        /// A method that will wait until the ThreadJob is complete.
        /// </summary>
        /// <returns></returns>
        public IEnumerator WaitForIsDone()
        {
            while (!IsDone)
            {
                yield return null;
            }
        }

        /// <summary>
        /// Executes the ThreadJob's Run() function and marks the ThreadJob as complete when it finishes.
        /// </summary>
        /// <param name="state">State parameter required for WaitCallback used in ThreadPool</param>
        private void ExecuteThread(object state)
        {
            Run();
            IsDone = true;
            Finish();
        }

        #endregion
    }
}
