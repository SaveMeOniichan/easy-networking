﻿using System;
using System.Collections.Generic;
using EasyNetworking.Exceptions;

namespace EasyNetworking.Utils
{
    /// <summary>
    /// This class recieves objects and distributes them to functions.
    /// </summary>
    public class Distributor<TKey, TMessage>
    {
        #region Fields and Properties

        /// <summary>
        /// Saved functions that will be used for distributing.
        /// </summary>
        private readonly IDictionary<TKey, Action<TMessage>> _distribution;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a Distributor
        /// </summary>
        public Distributor()
        {
            _distribution = new Dictionary<TKey, Action<TMessage>>();
        }

        /// <summary>
        /// Construcst a Distributor and sets it's backing dictionary of actions to the given dictionary.
        /// </summary>
        /// <param name="distributionDictionary">Dictionary to use for backing actions on how to distribute.</param>
        /// <exception cref="ArgumentNullException">Thrown when the dictionary provided is null.</exception>
        public Distributor(IDictionary<TKey, Action<TMessage>> distributionDictionary)
        {
            if (distributionDictionary == null)
            {
                throw new ArgumentNullException("distributionDictionary");
            }
            _distribution = distributionDictionary;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Distributes a message with the given key.
        /// </summary>
        /// <param name="key">Key of where to distribute it to.</param>
        /// <param name="message">Message to distribute.</param>
        /// <exception cref="NoDistributionPathException">Thrown if you try to pass a message into a path that hasn't been added yet.</exception>
        public void Distribute(TKey key, TMessage message)
        {
            Action<TMessage> action;
            if (!_distribution.TryGetValue(key, out action))
            {
                throw new NoDistributionPathException<TKey, TMessage>(key, message);
            }
            action(message);
        }

        /// <summary>
        /// Registers a distribution path to a given key.
        /// </summary>
        /// <param name="key">Key to add the path to.</param>
        /// <param name="distributionPath">Path to distribute messages to.</param>
        public void AddPath(TKey key, Action<TMessage> distributionPath)
        {
            _distribution[key] = distributionPath;
        }

        /// <summary>
        /// Unregisters the distribution path setting it to null.
        /// </summary>
        /// <param name="key">Path to unregister.</param>
        public void RemovePath(TKey key)
        {
            _distribution[key] = null;
        }

        #endregion
    }
}
