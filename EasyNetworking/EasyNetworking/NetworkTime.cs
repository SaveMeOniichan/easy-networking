﻿//using System;

//namespace EasyNetworking
//{
//    public class NetworkTime
//    {
//        /// <summary>
//        /// The current Network Time.
//        /// </summary>
//        public static float Time
//        {
//            get
//            {
//                if (!Initialized)
//                {
//                    // TODO throw not initialized
//                    throw new Exception();
//                }

//                if (IsServer)
//                {
//                    return ServerTimeFunction();
//                }
//                else
//                {
//                    return ClientTimeFunction() + ServerClientOffset;
//                }
//            }
//        }

//        /// <summary>
//        /// Function used by the Server to get time.
//        /// </summary>
//        internal static Func<float> ServerTimeFunction;

//        /// <summary>
//        /// Function used by the Client to get time.
//        /// </summary>
//        internal static Func<float> ClientTimeFunction;

//        /// <summary>
//        /// The current offset value.
//        /// </summary>
//        internal static float ServerClientOffset;

//        /// <summary>
//        /// If this is the server or not.
//        /// </summary>
//        internal static bool IsServer = false;

//        internal static bool Initialized = false;

//        public static void InitializeAsServer(Func<float> serverTime)
//        {
//            if (Initialized)
//            {
//                // TODO already intialized
//                throw new Exception();
//            }
//            IsServer = true;
//            ServerTimeFunction = serverTime;
//            Initialized = true;
//        }

//        public static void InitializeAsClient(Func<float> clientTime)
//        {
//            if (Initialized)
//            {
//                // TODO already intialized
//                throw new Exception();
//            }
//            IsServer = true;
//            ClientTimeFunction = clientTime;
//            Initialized = true;
//        }

//        internal void SynchronzieClient(float sent1, float received1, float sent2, float received2)
//        {

//        }

//        private struct Poll
//        {
//            public float Offset;
//            public float RoundTrip;

//            public Poll(float sent1, float received1, float sent2, float received2)
//            {
//                Offset = ((received1 - sent1) + (sent2 - received2)) / 2.0f;
//                RoundTrip = (received2 - sent1) - (sent2 - received1);
//            }
//        }
//    }
//}
