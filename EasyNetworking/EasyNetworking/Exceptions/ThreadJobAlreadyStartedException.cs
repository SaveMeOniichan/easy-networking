﻿using System;
using EasyNetworking.Utils;

namespace EasyNetworking.Exceptions
{
    /// <summary>
    /// Thrown when you try to start a ThreadJob that has already been started.
    /// </summary>
    public class ThreadJobAlreadyStartedException : Exception
    {
        /// <summary>
        /// The ThreadJob that you tried to start.
        /// </summary>
        public ThreadJob ThreadJob { get; private set; }

        /// <summary>
        /// Construcst a ThreadJobAlreadyStartedException thrown by a ThreadJob.
        /// </summary>
        /// <param name="threadJob">The ThreadJob that threw the exception.</param>
        public ThreadJobAlreadyStartedException(ThreadJob threadJob) : base("You cannot start a ThreadJob that has already been started.")
        {
            ThreadJob = threadJob;
        }
    }
}
