﻿using System;

namespace EasyNetworking.Exceptions
{
    /// <summary>
    /// Thrown when there is no path for distributing a message trying to be distributed.
    /// </summary>
    public class NoDistributionPathException<TKey, TMessage> : Exception
    {
        /// <summary>
        /// Key that had no path.
        /// </summary>
        public TKey Key { get; private set; }

        /// <summary>
        /// Message that was trying to be sent.
        /// </summary>
        public TMessage DistributedMessage { get; private set; }

        /// <summary>
        /// Construcst a NoDistributionPathException for the given key.
        /// </summary>
        /// <param name="key">Key that failed.</param>
        /// <param name="distributedMessage">Message that was trying to be distributed.</param>
        public NoDistributionPathException(TKey key, TMessage distributedMessage) : base("Failure to distribute message because there was no distribution path.")
        {
            Key = key;
            DistributedMessage = distributedMessage;
        }
    }
}
