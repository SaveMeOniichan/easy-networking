﻿using System;

namespace EasyNetworking.Exceptions
{
    /// <summary>
    /// Thrown if you try to do an action that requires a IConnection to be running.
    /// </summary>
    public class ConnectionNotRunningException : Exception
    {
        public ConnectionNotRunningException() : base() { }
    }
}
