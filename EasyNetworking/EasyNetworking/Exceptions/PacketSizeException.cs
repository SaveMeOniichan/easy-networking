﻿using System;

namespace EasyNetworking.Exceptions
{
    /// <summary>
    /// An exception thrown when a packet received in a TCP connection is too large.s
    /// </summary>
    public class PacketSizeException : Exception
    {
        /// <summary>
        /// The size of the packet received.
        /// </summary>
        public int PacketSize { get; private set; }

        /// <summary>
        /// The maximum allowed packet size.
        /// </summary>
        public int MaxPacketSize { get; private set; }

        /// <summary>
        /// Constructs a PacketSizeException.
        /// </summary>
        /// <param name="packetSize">The size of the packet received.</param>
        /// <param name="maxPacketSize">The maximum allowed packet size.</param>
        public PacketSizeException(int packetSize, int maxPacketSize) : base(string.Format("The packet size recieved was too large. Received {0} bytes but the maximum size is {1}.", packetSize, maxPacketSize))
        {
            PacketSize = packetSize;
            MaxPacketSize = maxPacketSize;
        }
    }
}
